package com.wardrobe.www.wardrobe.wardrobelist;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.wardrobe.www.R;
import com.wardrobe.www.favorites.data.FavoriteData;
import com.wardrobe.www.wardrobe.WardrobeModel;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by arvind on 3/16/16.
 */
public class ImageViewAdapter extends PagerAdapter {

    private static final int LOOPS_COUNT = 1000 ;
    private final Context context;
    private Cursor mCursor;
    private LayoutInflater mLayoutInflater;
    private ArrayList<WardrobeModel> items;
    private String uuid;


    public ImageViewAdapter(Context context, ArrayList<WardrobeModel> items) {
        this.context = context;
        this.items = items;
        mLayoutInflater = LayoutInflater.from(this.context);
    }


    @Override
    public int getCount() {
        if (items != null) {
            return items.size() ;
        }
        return 0;
    }

    public String getUUID()
    {
        return this.uuid ;
    }



    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

                Random randGen = new Random();
            View view = mLayoutInflater.inflate(R.layout.row_pager, container, false);
             this.uuid = items.get(position).uuid;
            String imageUri = items.get(position).name;
            ImageView imageView = (ImageView) view.findViewById(R.id.page);
            try {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(context));
                imageLoader.displayImage(imageUri, imageView);
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                imageView.setAdjustViewBounds(true);
                container.addView(view);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return view;


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setItems(ArrayList<WardrobeModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    public void add(WardrobeModel item) {
        this.items.add(item);
        notifyDataSetChanged();
    }
}
