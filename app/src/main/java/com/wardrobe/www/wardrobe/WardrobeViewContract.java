package com.wardrobe.www.wardrobe;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by arvind on 3/15/16.
 */
public class WardrobeViewContract {

    public interface View
    {
        Context getContext();

        void startActivityForResult(Intent intent, int request_pick_type);

        void save(ContentValues values, int outFitType);

        void setBottomEmpty();

        void setTopEmpty();

        void setTopCurrentItem(int i);

        void setBottomCurrentItem(int i);

        void setTopItems(ArrayList<WardrobeModel> size);

        boolean getFavoriteActiveState();

        void setFavoriteActiveState(boolean b);

        void onUpdate(ContentValues contentValues, int type, long id);

        void checkForData();

        void setTop(ArrayList<WardrobeModel> list);

        void setBottom(ArrayList<WardrobeModel> list);
    }




}
