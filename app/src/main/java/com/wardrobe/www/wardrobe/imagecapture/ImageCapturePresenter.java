package com.wardrobe.www.wardrobe.imagecapture;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.wardrobe.www.R;
import com.wardrobe.www.provider.WardrobeContract;
import com.wardrobe.www.wardrobe.BasePresenter;
import com.wardrobe.www.wardrobe.WardrobeModel;
import com.wardrobe.www.wardrobe.WardrobeViewContract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by arvind on 3/15/16.
 */
public class ImageCapturePresenter extends BasePresenter{


    private ArrayAdapter<String> mAdapter;
    private Uri mImageCaptureUri;
    private File mTemporaryImageFilePlaceHolder;
    private Bitmap mImagePostBitmap;

    private int outFitType;

    // Constants
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;

    // Null handler
    private DialogState mDialogStateListener = new DialogState() {
        @Override
        public void onCancel() {
        }
    };

    public ImageCapturePresenter(WardrobeModel model, WardrobeViewContract.View view) {
        super(model, view);
    }

    // Callback
    public static interface DialogState{
        void onCancel();
    }








    public void createImageChooseDialog(int type){
        Context context = view.getContext();
        mAdapter = new ArrayAdapter<>(context,
                android.R.layout.select_dialog_item,
                context.getResources().getStringArray(R.array.capture_image_items));
        mTemporaryImageFilePlaceHolder = new File(Environment.getExternalStorageDirectory(),
                "image"+getNameAsTimeStamp()+".png");
        this.outFitType = type;
        AlertDialog.Builder builder     = new AlertDialog.Builder(view.getContext());
        builder.setTitle(context.getString(R.string.select_image));
        builder.setAdapter( mAdapter, new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int item ) {
                if (item == 0) {
                    Intent intent    = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mImageCaptureUri = Uri.fromFile(mTemporaryImageFilePlaceHolder);

                    try {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);
                        view.startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    view.startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE );
                }
            }
        } );

        final AlertDialog dialog = builder.create();
        dialog.setOnCancelListener(mOnCancelListener);
        dialog.show();

    }

    public WardrobeModel saveImage(Context context, Bitmap b , int type){
        String name="image"+getNameAsTimeStamp()+".jpg";
        FileOutputStream out;
        try {
            out = context.openFileOutput(name, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
            model.name = name;
            model.isFavorited = false;
            model.type = this.outFitType;
            return model;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap getImageBitmap(Context context,String name){
        try{
            FileInputStream fis = context.openFileInput(name);
            Bitmap b = BitmapFactory.decodeStream(fis);
            fis.close();
            return b;
        }
        catch(Exception e){
        }
        return null;
    }

    public boolean deleteImageBitmap(Context context, String name){
        return context.deleteFile(name);
    }

    public Bitmap onActivityResultHandler(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return null;

        String path     = "";
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri);

            if (path == null)
                path = mImageCaptureUri.getPath();
            if (path != null)
                mImagePostBitmap = BitmapFactory.decodeFile(path, options);
        } else {
            path    = mImageCaptureUri.getPath();
            mImagePostBitmap = BitmapFactory.decodeFile(path,options);
        }
        Log.d("image_uri" , mImageCaptureUri.toString());
        model.name = mImageCaptureUri.toString();
        model.isFavorited = false;
        model.type = this.outFitType;
        model.uuid  = UUID.randomUUID().toString();
        view.save(model.getContentValues() , outFitType);
        view.checkForData();
        return mImagePostBitmap;

    }

    // Private methods
    private DialogInterface.OnCancelListener mOnCancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            mDialogStateListener.onCancel();
        }
    };

    private String getRealPathFromURI(Uri contentUri) {
        String [] proj      = {MediaStore.Images.Media.DATA};
        Cursor cursor       = ((Activity)view.getContext()).managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) return null;
        int column_index    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getNameAsTimeStamp(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        return dateFormat.format(new Date());
    }




}
