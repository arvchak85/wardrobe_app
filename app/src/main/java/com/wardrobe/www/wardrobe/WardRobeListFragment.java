package com.wardrobe.www.wardrobe;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wardrobe.www.R;
import com.wardrobe.www.database.DatabaseHelper;
import com.wardrobe.www.provider.WardrobeContract;
import com.wardrobe.www.wardrobe.imagecapture.ImageCapturePresenter;
import com.wardrobe.www.wardrobe.wardrobelist.ImageViewAdapter;
import com.wardrobe.www.wardrobe.wardrobelist.WardRobeListPresenter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by arvind on 3/16/16.
 */
public class WardRobeListFragment extends Fragment implements WardrobeViewContract.View {

    /**
     * Activity reference
     */
    Activity mActivity;

    /**
     * Views
     */
    @Bind(R.id.top)
    ViewPager topPager;
    @Bind(R.id.bottom)
    ViewPager bottomPager;
    @Bind(R.id.topEmpty)
    ImageView topEmptyView;
    @Bind(R.id.bottomEmpty)
    ImageView bottomEmptyView;
    @Bind(R.id.shuffle)
    FloatingActionButton shuffle;
    @Bind(R.id.favorite)
    FloatingActionButton addFavorites;
    @Bind(R.id.addTop)
    FloatingActionButton addTop;
    @Bind(R.id.addBottom)
    FloatingActionButton addBottom;


    private WardRobeListPresenter wardrobeListPresenter;
    private ImageCapturePresenter imageCapturePresenter;
    private ImageViewAdapter mAdapter;
    private ImageViewAdapter mBottomAdapter;
    private Loader<Cursor> topLoader;
    private Loader<Cursor> bottomLoader;

    private boolean isFavoriteActive;


    private ArrayList<WardrobeModel> wardrobeModelArrayList;
    private ArrayList<WardrobeModel> wardrobeModelBottomArrayList;


    /**
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WardrobeModel model = new WardrobeModel();
        wardrobeModelArrayList = new ArrayList<>();
        wardrobeModelBottomArrayList = new ArrayList<>();
        wardrobeListPresenter = new WardRobeListPresenter(model, this);
        imageCapturePresenter = new ImageCapturePresenter(model, this);
        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pager, container, false);
        ButterKnife.bind(this, view);
        mAdapter = new ImageViewAdapter(mActivity, null);
        mBottomAdapter = new ImageViewAdapter(mActivity, null);
        topPager.setAdapter(mAdapter);
        bottomPager.setAdapter(mBottomAdapter);
        topPager.addOnPageChangeListener(topChangeListner);
        bottomPager.addOnPageChangeListener(bottomChangeListener);
        wardrobeListPresenter.checkContentOnStart();
        topLoader = getLoaderManager().initLoader(0, null, topLoaderCallbacks);
        bottomLoader = getLoaderManager().initLoader(1, null, bottomLoaderCallbacks);
        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int topPosition = topPager.getCurrentItem();
        int bottomPosition = bottomPager.getCurrentItem();
        outState.putInt("top", topPosition);
        outState.putInt("bottom", bottomPosition);


    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            int topPosition = savedInstanceState.getInt("top");
            int bottomPosition = savedInstanceState.getInt("bottom");
            topPager.setCurrentItem(topPosition);
            bottomPager.setCurrentItem(bottomPosition);
            if (!wardrobeModelArrayList.isEmpty() && !wardrobeModelBottomArrayList.isEmpty()) {
                String topUuid = wardrobeModelArrayList.get(topPager.getCurrentItem()).id + "";
                String bottomUuid = wardrobeModelBottomArrayList.get(bottomPager.getCurrentItem()).id + "";
                wardrobeListPresenter.onScreenChange(topUuid, bottomUuid);
            }

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!wardrobeModelArrayList.isEmpty() && !wardrobeModelBottomArrayList.isEmpty()) {
            String topUuid = wardrobeModelArrayList.get(topPager.getCurrentItem()).id + "";
            String bottomUuid = wardrobeModelBottomArrayList.get(bottomPager.getCurrentItem()).id + "";
            wardrobeListPresenter.onScreenChange(topUuid, bottomUuid);
        }
    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        wardrobeListPresenter.checkContentOnStart();
        topLoader = getLoaderManager().initLoader(0, null, topLoaderCallbacks);
        bottomLoader = getLoaderManager().initLoader(1, null, bottomLoaderCallbacks);

    }*/

    @OnClick(R.id.addTop)
    public void onAddTopClick() {
        imageCapturePresenter.createImageChooseDialog(WardrobeModel.TYPE_TOP);
        wardrobeListPresenter.checkContentOnStart();
    }

    @OnClick(R.id.addBottom)
    public void onAddBottomClick() {
        imageCapturePresenter.createImageChooseDialog(WardrobeModel.TYPE_BOTTOM);
        wardrobeListPresenter.checkContentOnStart();
    }

    @OnClick(R.id.shuffle)
    public void onShuffleClick() {
        int size = wardrobeModelArrayList.size();
        wardrobeListPresenter.onShuffleClick(wardrobeModelArrayList.size(), wardrobeModelBottomArrayList.size(), topPager.getCurrentItem(), bottomPager.getCurrentItem());

    }

    @OnClick(R.id.favorite)
    public void onFavoriteClick() {
        wardrobeListPresenter.onFavoriteClick(wardrobeModelArrayList.get(topPager.getCurrentItem()), wardrobeModelBottomArrayList.get(bottomPager.getCurrentItem()));

    }

    ViewPager.OnPageChangeListener topChangeListner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

            if (state == ViewPager.SCROLL_STATE_IDLE) {
                if (!wardrobeModelArrayList.isEmpty() && !wardrobeModelBottomArrayList.isEmpty()) {
                    String topUuid = wardrobeModelArrayList.get(topPager.getCurrentItem()).id + "";
                    String bottomUuid = wardrobeModelBottomArrayList.get(bottomPager.getCurrentItem()).id + "";
                    wardrobeListPresenter.onScreenChange(topUuid, bottomUuid);
                }

            }

        }
    };


    ViewPager.OnPageChangeListener bottomChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

            if (state == ViewPager.SCROLL_STATE_IDLE) {
                if (!wardrobeModelArrayList.isEmpty() && !wardrobeModelBottomArrayList.isEmpty()) {
                    String topUuid = wardrobeModelArrayList.get(topPager.getCurrentItem()).id + "";
                    String bottomUuid = wardrobeModelBottomArrayList.get(bottomPager.getCurrentItem()).id + "";
                    wardrobeListPresenter.onScreenChange(topUuid, bottomUuid);

                }

            }

        }
    };


    LoaderCallbacks<Cursor> bottomLoaderCallbacks = new LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            CursorLoader loader = new CursorLoader(mActivity,
                    WardrobeContract.buildUriForOutfitType(WardrobeModel.TYPE_BOTTOM),
                    null,
                    null,
                    null,
                    null);
            return loader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            wardrobeModelBottomArrayList = wardrobeListPresenter.buildTopData(data);
            mBottomAdapter.setItems(wardrobeModelBottomArrayList);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            //mBottomAdapter.swapCursor(null);
        }
    };
    LoaderCallbacks<Cursor> topLoaderCallbacks = new LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            CursorLoader loader = new CursorLoader(mActivity,
                    WardrobeContract.buildUriForOutfitType(WardrobeModel.TYPE_TOP),
                    null,
                    null,
                    null,
                    null);
            return loader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            wardrobeModelArrayList = wardrobeListPresenter.buildTopData(data);
            mAdapter.setItems(wardrobeModelArrayList);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageCapturePresenter.onActivityResultHandler(requestCode, resultCode, data);
    }

    @Override
    public void save(ContentValues contentValues, int outFitType) {
        Uri uri = getActivity().getContentResolver().insert(WardrobeContract.buildUriForOutfitType(outFitType),
                contentValues);
        Log.d("uri", uri.toString());
    }


    @Override
    public void setBottomEmpty() {
        bottomEmptyView.setVisibility(View.VISIBLE);
        bottomPager.setVisibility(View.GONE);
        shuffle.setVisibility(View.GONE);
        addFavorites.setVisibility(View.GONE);
    }

    @Override
    public void setTopEmpty() {
        topEmptyView.setVisibility(View.VISIBLE);
        topPager.setVisibility(View.GONE);
        shuffle.setVisibility(View.GONE);
        addFavorites.setVisibility(View.GONE);
    }

    @Override
    public void setTopCurrentItem(int i) {

        topPager.setCurrentItem(i);
    }

    @Override
    public void setBottomCurrentItem(int i) {
        bottomPager.setCurrentItem(i, true);
    }

    @Override
    public void setTopItems(ArrayList<WardrobeModel> size) {

    }

    @Override
    public boolean getFavoriteActiveState() {
        return addFavorites.isActivated();
    }

    @Override
    public void setFavoriteActiveState(boolean b) {
        addFavorites.setActivated(b);
    }

    @Override
    public void onUpdate(ContentValues contentValues, int type, long id) {
        String[] whereArgsBottom = {id + ""};
        int update = getActivity().getContentResolver().update(WardrobeContract.buildUriForOutfitType(type),
                contentValues, DatabaseHelper.COL_ID + "=?", whereArgsBottom);
        Log.d("update", update + "");
    }

    @Override
    public void checkForData() {
        wardrobeListPresenter.checkContentOnStart();
    }

    @Override
    public void setTop(ArrayList<WardrobeModel> list) {
        mAdapter.setItems(list);
        topPager.setAdapter(mAdapter);
        topEmptyView.setVisibility(View.GONE);
        topPager.setVisibility(View.VISIBLE);
        shuffle.setVisibility(View.VISIBLE);
        addFavorites.setVisibility(View.VISIBLE);
    }

    @Override
    public void setBottom(ArrayList<WardrobeModel> list) {
        mBottomAdapter.setItems(list);
        bottomPager.setAdapter(mBottomAdapter);
        bottomEmptyView.setVisibility(View.GONE);
        bottomPager.setVisibility(View.VISIBLE);
        shuffle.setVisibility(View.VISIBLE);
        addFavorites.setVisibility(View.VISIBLE);
    }

    @Override
    public Context getContext() {
        return mActivity;
    }
}
