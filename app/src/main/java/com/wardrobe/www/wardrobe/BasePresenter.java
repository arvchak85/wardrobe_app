package com.wardrobe.www.wardrobe;

/**
 * Created by arvind on 3/15/16.
 */
public class BasePresenter {

    protected final WardrobeModel model;
    protected final WardrobeViewContract.View view;

    public BasePresenter(WardrobeModel model, WardrobeViewContract.View view) {
        this.model = model;
        this.view = view;
    }
}
