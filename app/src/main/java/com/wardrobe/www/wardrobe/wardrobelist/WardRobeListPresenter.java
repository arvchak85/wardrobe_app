package com.wardrobe.www.wardrobe.wardrobelist;

import android.content.Context;
import android.database.Cursor;

import com.wardrobe.www.WardrobeApplication;
import com.wardrobe.www.favorites.data.FavoriteData;
import com.wardrobe.www.provider.WardrobeContract;
import com.wardrobe.www.wardrobe.BasePresenter;
import com.wardrobe.www.wardrobe.WardrobeModel;
import com.wardrobe.www.wardrobe.WardrobeViewContract;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**
 * Created by arvind on 3/15/16.
 */
public class WardRobeListPresenter extends BasePresenter
{
    public WardRobeListPresenter(WardrobeModel model, WardrobeViewContract.View view) {
        super(model, view);
    }

    public void checkContentOnStart()
    {
        Context context = view.getContext();
        Cursor cursor = context.getContentResolver().query(WardrobeContract.buildUriForOutfitType(WardrobeModel.TYPE_TOP) , null , null , null , null);
        if(cursor.getCount() == 0){
            view.setTopEmpty();
        }
        else{
            ArrayList<WardrobeModel> list = buildTopData(cursor);
            view.setTop(list);
        }
        cursor = context.getContentResolver().query(WardrobeContract.buildUriForOutfitType(WardrobeModel.TYPE_BOTTOM) , null , null , null , null);
        if(cursor.getCount() == 0){
            view.setBottomEmpty();
        }
        else {
            ArrayList<WardrobeModel> list = buildTopData(cursor);
            view.setBottom(list);
        }
    }


    public ArrayList<WardrobeModel> buildTopData(Cursor data) {

        ArrayList<WardrobeModel> wardrobeList = new ArrayList<>();
        if(data!=null) {
            if (data.moveToLast()) {
                do {
                    WardrobeModel wardrobeModel = model.getModelFromCursor(data);
                    wardrobeList.add(wardrobeModel);
                } while (data.moveToPrevious());
            }

        }
        return wardrobeList;
    }

    public void onShuffleClick(int topSize, int bottomSize, int topPosition, int bottomPosition) {

        randomizeTop(topSize , topPosition);
        randomizeBottom(bottomSize,bottomPosition);


    }

    public void randomizeTop(int size , int position) {
        Random topRandom = new Random();
        if(size > 1) {
            int index = topRandom.nextInt(size);
            if(index == position)
                view.setTopCurrentItem(topRandom.nextInt(size));
            else
                view.setTopCurrentItem(index);
        }
    }

    public void randomizeBottom(int size , int position) {
        Random bottomRandpom = new Random();
        if(size > 1) {
            int index = bottomRandpom.nextInt(size);
            if(index == position) {
                view.setBottomCurrentItem(bottomRandpom.nextInt(size));
            }else{
                view.setBottomCurrentItem(index);
            }
        }
    }


    public void onFavoriteClick(WardrobeModel topModel , WardrobeModel bottomModel) {

       boolean active =  view.getFavoriteActiveState();
        FavoriteData data;

        if(active){
            //update false ;
            data = new FavoriteData(topModel.id+"" , bottomModel.id+"");
            if(WardrobeApplication.dataList.favorites.indexOf(data) > -1){
                WardrobeApplication.dataList.favorites.remove(WardrobeApplication.dataList.favorites.indexOf(data));
            }
        }
        else{

            data = new FavoriteData(topModel.id+"" , bottomModel.id+"");
            WardrobeApplication.dataList.favorites.add(data);
            view.onUpdate(topModel.getContentValues() , topModel.type , topModel.id);
            view.onUpdate(bottomModel.getContentValues(), bottomModel.type , bottomModel.id);
            //update true;
        }
        WardrobeApplication.setData(WardrobeApplication.dataList);
        view.setFavoriteActiveState(!active);
    }

    public void onScreenChange(String topUuid, String bottomUuid) {
        FavoriteData data = new FavoriteData(topUuid, bottomUuid);
        int index = WardrobeApplication.dataList.favorites.indexOf(data);
        boolean isFavoriteActive;
        if (index > -1)
            isFavoriteActive = true;
        else
            isFavoriteActive = false;
        view.setFavoriteActiveState(isFavoriteActive);
    }
}
