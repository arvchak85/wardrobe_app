package com.wardrobe.www.wardrobe;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.wardrobe.www.database.DatabaseHelper;
import com.wardrobe.www.provider.WardrobeContract;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by arvind on 3/15/16.
 */

/**
 * Tech debt : Serializable will be and must be changed to parcelable later.
 */
public class WardrobeModel extends WardrobeContract implements Serializable {

    public final static int TYPE_TOP = 1;
    public final static int TYPE_BOTTOM = 2;


    public long id ;
    public String name;
    public boolean isFavorited;
    public int type;
    public String uuid;
    private ContentValues contentValues;

    public WardrobeModel() {
        this.contentValues = new ContentValues();
    }

    public ContentValues getContentValues() {
        contentValues.put(COLUMN_URI, this.name);
        contentValues.put(COLUMN_TYPE, this.type);
        contentValues.put(COLUMN_IS_FAVORITED, this.isFavorited);
        contentValues.put(COLUMN_FAVORITE_UUID , this.uuid);
        return contentValues;
    }

    public WardrobeModel getModelFromCursor(Cursor cursor) {
        WardrobeModel model = new WardrobeModel();
        if(cursor.getCount()> 0) {
            model.id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COL_ID));
            model.name = cursor.getString(cursor.getColumnIndex(COLUMN_URI));
            model.isFavorited = cursor.getInt(cursor.getColumnIndex(COLUMN_IS_FAVORITED)) == 0 ? false : true;
            model.type = cursor.getInt(cursor.getColumnIndex(COLUMN_TYPE));
            model.uuid = cursor.getString(cursor.getColumnIndex(COLUMN_FAVORITE_UUID));
        }
        return model;
    }

}
