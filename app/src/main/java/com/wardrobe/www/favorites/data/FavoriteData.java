package com.wardrobe.www.favorites.data;

/**
 * Created by arvind on 3/16/16.
 */
public class FavoriteData {
    String uuidTop;
    String uuidBottom;


    public FavoriteData(String uuidTop, String uuidBottom) {
        this.uuidTop = uuidTop;
        this.uuidBottom = uuidBottom;
    }

    public FavoriteData() {

    }

    @Override
    public boolean equals(Object o) {
        FavoriteData obj = (FavoriteData) o;
        if (this.uuidTop == null || obj.uuidTop == null)
            return false;
        if (this.uuidBottom == null || obj.uuidBottom == null)
            return false;
        return this.uuidTop.equals(obj.uuidTop) && this.uuidBottom.equals(obj.uuidBottom);
    }

    @Override
    public String toString() {
        return "Top -> "+this.uuidTop + " Bottom ->"+this.uuidBottom;
    }
}
