package com.wardrobe.www;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.wardrobe.www.favorites.data.FavoriteDataList;
import com.wardrobe.www.service.NotifyService;

import java.util.Calendar;

/**
 * Created by arvind on 3/16/16.
 */
public class WardrobeApplication extends Application {

    private static final String PREFERENCES_KEY = "key";
    private static final String KEY_PREFERENCE_DATA = "data";
    public static FavoriteDataList dataList ;
    private static SharedPreferences preferences;
    private static  SharedPreferences.Editor editor;
    private static Gson gson;
    private PendingIntent pendingIntent;

    @Override
    public void onCreate() {
        super.onCreate();
        gson = new Gson();
        preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        editor = preferences.edit();
        dataList = getData();
        subscribeNotification();

    }

    public static void setData(FavoriteDataList data) {
        dataList = data;
        String json = new Gson().toJson(data);
        editor.putString(KEY_PREFERENCE_DATA, json);
        editor.commit();
    }


    public static FavoriteDataList getData() {

        if (preferences != null) {
            String json = preferences.getString(KEY_PREFERENCE_DATA,
                    null);

            if (json!=null && !"null".equalsIgnoreCase(json)) {
                dataList = gson.fromJson(json, FavoriteDataList.class);
            } else {
                dataList = new FavoriteDataList();
            }
        } else {
            dataList = new FavoriteDataList();
        }
        return dataList;

    }

    public void subscribeNotification()
    {
        Intent myIntent = new Intent(this , NotifyService.class);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        pendingIntent = PendingIntent.getService(this.getApplicationContext(), 0, myIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 06);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 0L ,pendingIntent);
    }

}
