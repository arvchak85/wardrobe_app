package com.wardrobe.www.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.wardrobe.www.provider.WardrobeContract;

/**
 * Database helper .
 * 
 *
 * @author arvind
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	/**
	 * id column present in all tables
	 */
	public static final String COL_ID = "_id";

	/**
	 * id column position must always be the first
	 */

	public static final int POS_ID = 0;
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "wardrobe.db";
	private static DatabaseHelper sInstance;
	private String table;
	private String createTable;

	public static DatabaseHelper getInstance(Context context) {

		if (sInstance == null) {
			sInstance = new DatabaseHelper(context.getApplicationContext());
		}
		return sInstance;
	}



	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.setLockingEnabled(false);
			String recent_search = WardrobeContract.CREATE_WARDROBE_TABLE_STATEMENT;
			db.execSQL(recent_search);

		} finally {
			db.setLockingEnabled(true);
		}


	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		// this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + WardrobeContract.TABLE_NAME);
		onCreate(db);

	}








}
