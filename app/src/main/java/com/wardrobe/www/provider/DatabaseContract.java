package com.wardrobe.www.provider;

import android.net.Uri;

/**
 * Created by arvind on 3/15/16.
 */
public class DatabaseContract {

    public static final String AUTHORITY = "com.wardrobe.www";
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);
}
