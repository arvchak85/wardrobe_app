package com.wardrobe.www.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.wardrobe.www.R;
import com.wardrobe.www.database.DatabaseHelper;
import com.wardrobe.www.wardrobe.WardrobeModel;

/**
 * @author arvind
 */
public class WardrobeContentProvider extends ContentProvider {

    private static final int TOPS = 1;
    private static final int BOTTOM = 2;
    public static final int TOPRANDOM = 3;
    public static final int BOTTOMRANDOM = 4;
    private static UriMatcher mUriMatcher;

    static {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(DatabaseContract.AUTHORITY,
                WardrobeContract.SHIRT_PATH + "/*",
                TOPS);
        mUriMatcher.addURI(DatabaseContract.AUTHORITY,
                WardrobeContract.SHIRT_PATH + "/*",
                TOPRANDOM);

        mUriMatcher.addURI(DatabaseContract.AUTHORITY,
                WardrobeContract.PANT_PATH + "/*",
                BOTTOMRANDOM);
        mUriMatcher.addURI(DatabaseContract.AUTHORITY,
                WardrobeContract.PANT_PATH + "/*",
                BOTTOM);

    }

    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHelper(getContext());
        return mDatabaseHelper != null;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        int match = mUriMatcher.match(uri);
        SQLiteDatabase sqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        switch (match) {

            case TOPS:
                String whereClause = WardrobeContract.COLUMN_TYPE + "=?";
                String[] whereArgs = {WardrobeModel.TYPE_TOP + ""};
                cursor = sqLiteDatabase.query(WardrobeContract.TABLE_NAME,
                        projection, whereClause, whereArgs, null, null, null);
                break;
            case TOPRANDOM:
                String whereClauseTopRandom = WardrobeContract.COLUMN_TYPE + "=?";
                String[] whereArgsToprandom = {WardrobeModel.TYPE_TOP + ""};
                cursor = sqLiteDatabase.query(WardrobeContract.TABLE_NAME,
                        projection, whereClauseTopRandom, whereArgsToprandom, null, null, null);
                break;
            case BOTTOMRANDOM:
                String whereClauseBottomRandom = WardrobeContract.COLUMN_TYPE + "=?";
                String[] whereArgsBottomrandom = {WardrobeModel.TYPE_BOTTOM + ""};
                cursor = sqLiteDatabase.query(WardrobeContract.TABLE_NAME,
                        projection, whereClauseBottomRandom, whereArgsBottomrandom, null, null, "RANDOM() ");
                break;
            case BOTTOM:
                String whereClauseBottom = WardrobeContract.COLUMN_TYPE + "=?";
                String[] whereArgsBottom = {WardrobeModel.TYPE_BOTTOM + ""};
                cursor = sqLiteDatabase.query(WardrobeContract.TABLE_NAME,
                        projection, whereClauseBottom, whereArgsBottom, null, null, null);
                break;

            default:
                throw new UnsupportedOperationException(getContext().getString(R.string.uri_not_defined) + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = mUriMatcher.match(uri);
        SQLiteDatabase sqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        long id = sqLiteDatabase.insert(WardrobeContract.TABLE_NAME, null, values);
        Log.d("id", id + "");
        uri = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = mUriMatcher.match(uri);
        int deletedRowId = 0;
        switch (match) {
            case TOPS:
                SQLiteDatabase sqLiteDatabase = mDatabaseHelper.getWritableDatabase();
                deletedRowId = sqLiteDatabase
                        .delete(WardrobeContract.TABLE_NAME, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            default:
                throw new UnsupportedOperationException(getContext().getString(R.string.uri_not_defined) + uri);
        }

        return deletedRowId;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int updatedRows = 0;
        SQLiteDatabase sqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        updatedRows = sqLiteDatabase.update(WardrobeContract.TABLE_NAME,
                values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return updatedRows;
    }


}
