package com.wardrobe.www.provider;

import android.net.Uri;
import android.util.Log;

import com.wardrobe.www.database.DatabaseHelper;
import com.wardrobe.www.wardrobe.WardrobeModel;

/**
 * Created by arvind on 3/15/16.
 */
public class WardrobeContract {


    public static final String COLUMN_URI = "column_uri";
    public static final String COLUMN_IS_FAVORITED = "column_is_favorited";
    public static final String COLUMN_TYPE = "column_type" ;
    public static final String COLUMN_FAVORITE_UUID = "column_favorite_uuid";
    public static final String TABLE_NAME = "wardrobe_table";

    public static final String SHIRT_PATH = "shirt";
    public static final String PANT_PATH = "pant";

    public static final String CREATE_WARDROBE_TABLE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "( "
            + DatabaseHelper.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_URI + " TEXT NOT NULL, "
            + COLUMN_IS_FAVORITED + " INTEGER, "
            + COLUMN_TYPE + " INTEGER, "
            +COLUMN_FAVORITE_UUID +" TEXT)";

    public static Uri buildUriForOutfitType(int viewType) {
        String path;
        if (viewType == WardrobeModel.TYPE_TOP){
            path = SHIRT_PATH;
        }
        else{
            path = PANT_PATH;
        }
        Log.d("build_uri" ,  DatabaseContract.BASE_URI.buildUpon().appendPath(path).appendPath(viewType+"").build().toString());
        return DatabaseContract.BASE_URI.buildUpon().appendPath(path).appendPath(viewType + "").build();
    }


    public static Uri buildUriForOutfitRandome(int viewType) {
        String path;
        String segment ;
        String randomSegment;
        if (viewType == WardrobeModel.TYPE_TOP){
            path = SHIRT_PATH;
            segment = "TopRandom";
            randomSegment =WardrobeContentProvider.TOPRANDOM+"";
        }
        else{
            path = PANT_PATH;
            segment = "BottomRandom";
            randomSegment =WardrobeContentProvider.BOTTOMRANDOM+"";
        }
        Log.d("build_uri", DatabaseContract.BASE_URI.buildUpon().appendPath(path).appendPath(randomSegment).build().toString());
        return DatabaseContract.BASE_URI.buildUpon().appendPath(path).appendPath(randomSegment).build();
    }

}
